import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NuevoService {

  // Escribir la url
  public URL = 'https://rickandmortyapi.com/api';

  // inyectar el metodo HttpClient
  constructor(private _http: HttpClient) { }

  // escribir metodo para hacer peticion
  getCharacters(){

    const url = `${this.URL}/character`;

    return this._http.get(url);
  }
}
