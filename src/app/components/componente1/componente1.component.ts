import { Component, OnInit } from '@angular/core';
import { NuevoService } from 'src/app/Servicios/nuevo.service';

@Component({
  selector: 'app-componente1',
  templateUrl: './componente1.component.html',
  styleUrls: ['./componente1.component.css']
})
export class Componente1Component implements OnInit {

  public personajes : any[] = [];


  constructor(private _servicio: NuevoService) { }

  ngOnInit(): void {
    this.getPersonajes()
  }

  getPersonajes(){

    this.personajes = [];
    
     this._servicio.getCharacters().subscribe((data: any) => {
       console.log(data);

       this.personajes = data.results;

       console.log(this.personajes);

     })
    
  }

}
